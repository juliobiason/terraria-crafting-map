all: terraria104.dot.png terraria104.neato.png terraria104.fdp.png terraria104.twopi.png terraria104.circo.png terraria104.sfdp.png

terraria104.dot.png: terraria104.dot
	@echo "Dot"
	@dot terraria104.dot -oterraria104.dot.png -Tpng -s100

terraria104.neato.png: terraria104.dot
	@echo "Neato"
	@neato terraria104.dot -oterraria104.neato.png -Tpng -s100

terraria104.fdp.png: terraria104.dot
	@echo "Fdp"
	@fdp terraria104.dot -oterraria104.fdp.png -Tpng -s100

terraria104.twopi.png: terraria104.dot
	@echo "Twopi"
	@twopi terraria104.dot -oterraria104.twopi.png -Tpng -s100

terraria104.circo.png: terraria104.dot
	@echo "Circo"
	@circo terraria104.dot -oterraria104.circo.png -Tpng -s100

terraria104.sfdp.png: terraria104.dot
	@echo "Sfdp"
	@sfdp terraria104.dot -oterraria104.sfdp.png -Tpng -s100

.PHONY: all