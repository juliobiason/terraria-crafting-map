About this
==========

In my attempt to check what you can craft in 
`Terraria <http://terraria.org>`_, I decided to make a map of everything
you can craft. For that, I used the `Crafting page
<http://terraria.wikia.com/wiki/Crafting>`_ in the `Wikia Wiki
<http://terraria.wikia.com/>`_ for Terraria and `Graphviz
<http://www.graphviz.org/>`_.

Nodes are the materials/objects you can gather or craft. The lines point
from source to target. Over the lines there are pairs of "required ->
resulting". For example, most walls (if not all) take one element and
create 4; thus, those elements will have a line with "1 -> 4". I'm still
thinking if it's useful, as walls are one of the few things that create
more elements than their sources.
